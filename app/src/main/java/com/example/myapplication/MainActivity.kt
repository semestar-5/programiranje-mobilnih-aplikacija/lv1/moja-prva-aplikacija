package com.example.myapplication

import android.graphics.Color
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.TextView

class MainActivity : AppCompatActivity() {

    lateinit var button2 : Button
    lateinit var button1: Button
    //moze se kasnije dodavati vrijednost sa lateinit
    lateinit var tekstTv : TextView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        button2=findViewById<Button>(R.id.btnDobrodosli);
        button1=findViewById<Button>(R.id.btnDovidenja);
        tekstTv= findViewById<TextView>(R.id.textView);

        button2.setOnClickListener{
            tekstTv.setText("Dobar dan!");
            button2.setBackgroundColor(Color.GREEN);
        }
        button1.setOnClickListener{
            tekstTv.setText("Dovidenja!");
            button1.setBackgroundColor(Color.RED) ;
        }
    }
}